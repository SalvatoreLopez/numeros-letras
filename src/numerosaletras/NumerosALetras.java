/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerosaletras;

import controlador.numerosALetras;

/**
 *
 * @author salvatore
 */
public class NumerosALetras {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String cantidad = "1093999", numeroLetras="";
        if (cantidad.contains(".")) {
            String numeros[] = cantidad.split("\\.");
            if (numeros.length == 2) {
                String enteros = numeros[0];
                String decimales = ((numeros[1]).length() == 1) ? (numeros[1])+"0" : (numeros[1]);
                System.out.println(numerosALetras.numeroALetras(Integer.parseInt(enteros)));
            }
        }else{
             System.out.println(numerosALetras.numeroALetras(Integer.parseInt(cantidad)));
        }
    }
    
}
