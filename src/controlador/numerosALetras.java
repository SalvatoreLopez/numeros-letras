package controlador;

public class numerosALetras {
    public static String numeroALetras(int numero){
        String cadena = new String();
        //millones
        if ((numero/1000000)>0) {
            if((numero/1000000)==1){
                cadena ="Un millón "+numeroALetras(numero%1000000);
            }else{
                cadena = numeroALetras(numero/1000000)+"Millones "+numeroALetras(numero%1000000);
            }
        } //miles
        else if((numero/1000)>0){
            if((numero/1000)==1){
                cadena ="Mil "+numeroALetras(numero%1000);
            }else{
                cadena = numeroALetras(numero/1000)+"Mil "+numeroALetras(numero%1000);
            }
            //centenas
        }else if((numero/100)>0){
            switch (numero/100) {
                case 1:
                    if((numero%100)==0){
                        cadena ="Cien ";
                    }else{
                        cadena = "Ciento "+numeroALetras(numero%100);
                    }   break;
            //decenas
                case 2:
                    cadena = "Doscientos "+numeroALetras(numero%100);
                    break;
                case 3:
                    cadena = "Trescientos "+numeroALetras(numero%100);
                    break;
                case 4:
                    cadena = "cuatrocientos "+numeroALetras(numero%100);
                    break;
                case 5:
                    cadena = "Quinientos "+numeroALetras(numero%100);
                    break;
                case 6:
                    cadena = "Seiscientos "+numeroALetras(numero%100);
                    break;
                case 7:
                    cadena = "Setecientos "+numeroALetras(numero%100);
                    break;
                case 8:
                    cadena = "Ochocientos "+numeroALetras(numero%100);
                    break;
                case 9:
                    cadena = "Novecientos "+numeroALetras(numero%100);
                    break;
                default:
                    cadena = numeroALetras(numero/100)+"cientos "+numeroALetras(numero%100);
                    break;
            }
        }else if((numero/10)>0){
            switch((int)(numero/10)){
                case 1:
                    switch((int)(numero%10)){
                        case 0: cadena = "Diez " ; break;
                        case 1: cadena = "Once " ; break;
                        case 2: cadena = "Doce " ; break;
                        case 3: cadena = "Trece " ; break;
                        case 4: cadena = "Catorce " ; break;
                        case 5: cadena = "Quince " ; break;
                        case 6: cadena = "Dieciséis " ; break;
                        case 7: cadena = "Diecisiete " ; break;
                        case 8: cadena = "Dieciocho " ; break;
                        case 9: cadena = "Diecinueve " ; break;
                    }
                break;

                case 2:
                    switch((int)(numero%10)){
                        case 0 : cadena = "Veinte "; break;
                        default: cadena = "Veinti " + numeroALetras(numero%10);  break;
                    }
                break;

                case 3:
                    switch((int)(numero%10)){
                        case 0 : cadena = "Treinta "; break;
                        default: cadena = "Treinta y " + numeroALetras(numero%10);  break;
                    }
                break;

                case 4:
                    switch((int)(numero%10)){
                        case 0 : cadena = "Cuarenta "; break;
                        default: cadena = "Cuarenta y " + numeroALetras(numero%10);  break;
                    }
                break;

                case 5:
                    switch((int)(numero%10)){
                        case 0 : cadena = "Cincuenta "; break;
                        default: cadena = "Cincuenta y " + numeroALetras(numero%10);  break;
                    }
                break;

                case 6:
                    switch((int)(numero%10)){
                        case 0 : cadena = "Sesenta "; break;
                        default: cadena = "Sesenta y " + numeroALetras(numero%10);  break;
                    }
                break;

                case 7:
                    switch((int)(numero%10)){
                        case 0 : cadena = "Setenta "; break;
                        default: cadena = "Setenta y " + numeroALetras(numero%10);  break;
                    }
                break;

                case 8:
                    switch((int)(numero%10)){
                        case 0 : cadena = "Ochenta "; break;
                        default: cadena = "Ochenta y " + numeroALetras(numero%10);  break;
                    }
                break;

                case 9:
                    switch((int)(numero%10)){
                        case 0 : cadena = "Noventa "; break;
                        default: cadena = "Noventa y " + numeroALetras(numero%10);  break;
                    }
                break;
            }
        }else{
            switch((int)(numero)){
                //case 0: cadena=" Cero "; break;
                case 1: cadena="Uno "; break;
                case 2: cadena="Dos "; break;
                case 3: cadena="Tres "; break;
                case 4: cadena="Cuatro "; break;
                case 5: cadena="Cinco "; break;
                case 6: cadena="Seis "; break;
                case 7: cadena="Siete "; break;
                case 8: cadena="Ocho "; break;
                case 9: cadena="Nueve "; break;
            }
        }
        return cadena;
    }
}